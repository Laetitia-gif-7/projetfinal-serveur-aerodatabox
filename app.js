const express = require('express')
const https = require('https')
const app = express()
const request = require('request')
const mongoose = require('mongoose')
const cron = require('node-cron')
const cors = require('cors');
const urlocal='mongodb://localhost:27017'
const url= process.env.URL_MONGO || urlocal;
const bodyParser = require('body-parser');
const Flight = require("./flight");
const { response } = require('./app');
const MONGO_URL = process.env.MONGO_URL;
require('dotenv').config({path:"./.env"})
const PORT = process.env.PORT



//let options = {json: true};
app.use(bodyParser.json());
app.use(cors({origin: '*'}))
app.get('/', (req, res) => { 
    res.send('Serveur is running')
});

app.listen(PORT);

//connection à MongoDB
mongoose.connect('mongodb+srv://laetitia:laetitia@aerodataboxproject4.ezdkr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
 {   //pour recuperer le 1er paramètre, aller sur mongodb, cliquer sur connect > connect your application
     useNewUrlParser: true, useUnifiedTopology:true
    }).then(()=>{
        console.log("connexion success !");//si ça fonctionne on affiche cela dans la console
    }).catch((error) =>{
        console.log(error); //sinon on affiche l'erreur
    });

app.get('/flights', (req, res) =>{

    Flight.find({})
    .then((flights)=>{
        console.log(flights);
        return res.status(200).json({flights})
        
    }) 
    .catch((error)=>{
        return res.status(400).json({error})
    })

})





var XMLHttpRequest = require('xhr2');
var xhr = new XMLHttpRequest();
const Http = require('xmlhttprequest').XMLHttpRequest;

var options = {
    method: 'GET',
    url: `https://projet-final-robot-aero.herokuapp.com/flights/`
  };

  app.get("/flights/:number", (req, res) =>{
    
    var number = req.params.number;
    
    Flight.find({})
    .then((flights)=>{
        
        let flightByNumber= flights.filter(function(f){
            return f.number == number;
        })
       
        if(flightByNumber != 0){
        console.log("Ce vol est deja enregistré dans la bdd");
        return res.status(200).json({flightByNumber})
        }else{
        console.log("ce vol n'est pas enregistré");
        
        options.url = `https://projet-final-robot-aero.herokuapp.com/flights/${number}`;
        console.log(options.url);

        
        xhr.open("GET", options.url, true);
        xhr.send(null);
        xhr.unsent(); 
     }
     
    
    })
    
   
    .catch((error)=>{
        return res.status(400).json({error})
    })


}) 


    



